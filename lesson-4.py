def lesson4(
    *args: tuple,
) -> list:
    print(
        type(args)
    )
    x = set({})
    y = list
    for (
        item
    ) in args:
        x = set(
            item
        ) | set(
            item
        )
        print(
            type(x)
        )
    y = list(set(x))
    return list([y])


print(
    lesson4(
        tuple(
            [
                1,
                4,
                5,
                6,
            ]
        ),
        tuple(
            [
                3,
                1,
                3,
                4,
            ]
        ),
        tuple(
            [
                3,
                5,
                6,
                7,
            ]
        ),
    )
)
